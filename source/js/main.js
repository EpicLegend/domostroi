"use strict";
//= ../node_modules/lazysizes/lazysizes.js

//= ../node_modules/jquery/dist/jquery.js
//= ../node_modules/bootstrap/js/dist/util.js
//= ../node_modules/bootstrap/js/dist/alert.js
//= ../node_modules/bootstrap/js/dist/button.js
//= ../node_modules/bootstrap/js/dist/carousel.js
//= ../node_modules/bootstrap/js/dist/collapse.js
//= ../node_modules/bootstrap/js/dist/dropdown.js
//= ../node_modules/bootstrap/js/dist/modal.js
//= ../node_modules/bootstrap/js/dist/tooltip.js
//= ../node_modules/bootstrap/js/dist/popover.js
//= ../node_modules/bootstrap/js/dist/scrollspy.js
//= ../node_modules/bootstrap/js/dist/tab.js
//= ../node_modules/bootstrap/js/dist/toast.js

//= library/jquery-ui.js
//= library/jquery.ui.touch-punch.min.js
//= library/slick.js
//= library/wow.js
//= library/jquery.scrollUp.js
//= sliders.js


//= ../node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.js



$(document).ready(function () {
	/*------ ScrollUp -------- */
	jQuery.scrollUp({
		scrollText: '<i class="icon-arrow-up"></i>',
		easingType: 'linear',
		scrollSpeed: 900,
		animation: 'fade'
	});

	/*-------------------------
	Category active
	--------------------------*/
	jQuery('.categori-show').on('click', function(e) {
		e.preventDefault();
		jQuery('.categori-hide , .categori-hide-2').slideToggle(900);
	});

	function miniCart() {
		var navbarTrigger = jQuery('.cart-active'),
		endTrigger = jQuery('.cart-close'),
		container = jQuery('.sidebar-cart-active'),
		wrapper = jQuery('.main-wrapper');

		wrapper.prepend('<div class="body-overlay"></div>');

		navbarTrigger.on('click', function(e) {
			e.preventDefault();
			container.addClass('inside');
			wrapper.addClass('overlay-active');
			$("body").css("overflow", "hidden");
		});

		endTrigger.on('click', function() {
			container.removeClass('inside');
			wrapper.removeClass('overlay-active');
			$("body").css("overflow", "auto");
		});

		jQuery('.body-overlay').on('click', function() {
			container.removeClass('inside');
			wrapper.removeClass('overlay-active');
			$("body").css("overflow", "auto");
		});
	};
	miniCart();

	/*------ Wow Active ----*/
	new WOW().init();

	/* Sidebar menu Active  */
	function mobileHeaderActive() {
		var navbarTrigger = $('.mobile-header-button-active'),
			endTrigger = $('.sidebar-close'),
			container = $('.mobile-header-active'),
			wrapper4 = $('.main-wrapper');

		wrapper4.prepend('<div class="body-overlay-1"></div>');

		navbarTrigger.on('click', function(e) {
			e.preventDefault();
			container.addClass('sidebar-visible');
			wrapper4.addClass('overlay-active-1');
			$("body").css("overflow", "hidden");
		});

		endTrigger.on('click', function() {
			container.removeClass('sidebar-visible');
			wrapper4.removeClass('overlay-active-1');
			$("body").css("overflow", "auto");
		});

		$('.body-overlay-1').on('click', function() {
			container.removeClass('sidebar-visible');
			wrapper4.removeClass('overlay-active-1');
			$("body").css("overflow", "auto");
		});
	};
	mobileHeaderActive();



	/*---------------------
	Price range
	--------------------- */
	var sliderrange = jQuery('#slider-range');
	// var amountprice = jQuery('#amount');
	jQuery(function() {
		sliderrange.slider({
			range: true,
			min: 16,
			max: 400,
			values: [0, 300],
			slide: function(event, ui) {
			// amountprice.val("$" + ui.values[0] + " - $" + ui.values[1]);
			}
		});
		// amountprice.val("$" + sliderrange.slider("values", 0) + " - $" + sliderrange.slider("values", 1));
	});


	/*----------------------------
	Cart Plus Minus Button
	------------------------------ */
	var CartPlusMinus = jQuery('.cart-plus-minus');
	CartPlusMinus.prepend('<div class="dec qtybutton">-</div>');
	CartPlusMinus.append('<div class="inc qtybutton">+</div>');
	jQuery(".qtybutton").on("click", function() {
		var $button = jQuery(this);
		var oldValue = $button.parent().find("input").val();
		if ($button.text() === "+") {
			var newVal = parseFloat(oldValue) + 1;
		} else {
		// Don't allow decrementing below zero
			console.log(oldValue);
			if (oldValue > 0) {
				var newVal = parseFloat(oldValue) - 1;
			} else {
				newVal = 0;
			}
		}
		$button.parent().find("input").val(newVal);
		inputChangeCount();
	});
	$(".cart-plus-minus .cart-plus-minus-box").change(function () {
		inputChangeCount();
	});
	function inputChangeCount() {
		console.log("chenge number");
	}


	$(".team-slider").slick({
		lazyLoad: 'ondemand',
	  centerMode: true,
	  centerPadding: '60px',
	  slidesToShow: 4,
	  responsive: [
	    {
	      breakpoint: 768,
	      settings: {
	        arrows: false,
	        centerMode: true,
	        centerPadding: '40px',
	        slidesToShow: 3
	      }
	    },
	    {
	      breakpoint: 480,
	      settings: {
	        arrows: false,
	        centerMode: true,
	        centerPadding: '40px',
	        slidesToShow: 1
	      }
	    }
	  ]
	});
});
